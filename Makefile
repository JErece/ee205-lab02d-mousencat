###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 02c - Mouse 'N Cat - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Mouse 'N Cat Program
###
### @author  Justin Erece <erece8@hawaii.edu>
### @date    25 January 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = mouseNcat

all: $(TARGET)

mouseNcat: mouseNcat.c
	$(CC) $(CFLAGS) -o $(TARGET) mouseNcat.c

clean:
	rm -f $(TARGET) *.o

